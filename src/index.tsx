import './styles/all.scss';
import './index.scss';

import React from 'react';
import { createRoot } from 'react-dom/client';

import App from './app/app.index';

const container = document.getElementById('root');
const root = createRoot(container);
root.render(<App />);