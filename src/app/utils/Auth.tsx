﻿import React from 'react';
import { Navigate, Outlet } from 'react-router-dom';

export const AuthWrapper = () => {
    const blogAuth = window.localStorage.getItem('blog-auth')

    return <>{blogAuth ? <Navigate to="" /> : <Outlet />}</>;
}

export const PrivateWrapper = () => {
    const blogAuth = window.localStorage.getItem('blog-auth')

    return <>{blogAuth ? <Outlet /> : <Navigate to="/sign-in" />}</>;
}
