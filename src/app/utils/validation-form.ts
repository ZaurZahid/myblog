import * as Yup from 'yup';

export const signInSchema = Yup.object().shape({
    email: Yup.string().required('Email is required').email('Email is invalid'),
    password: Yup.string()
        .required('Password is required')
        .min(6, 'Password must be at least 6 characters')
        .max(40, 'Password must not exceed 40 characters'),
});

export const signUpSchema = Yup.object({
    name: Yup.string()
        .required('Name is required')
        .min(2, 'Name must be at least 2 characters')
        .max(40, 'Name must not exceed 40 characters'),
}).concat(signInSchema);

export const savePostSchema = Yup.object({
    title: Yup.string()
        .required('Title is required')
        .min(10, 'Title must be at least 10 characters')
        .max(40, 'Title must not exceed 40 characters'),
    content: Yup.string()
        .required('Content is required')
        .min(10, 'Content must be at least 10 characters')
        .max(200, 'Content must not exceed 200 characters'),
});
