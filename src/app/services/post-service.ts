import {
    addDoc,
    collection,
    deleteDoc,
    doc,
    getDocs,
    getFirestore,
    setDoc,
} from 'firebase/firestore';

import { IPost, IPostFormInputs } from '../models/post-model';
import { firebaseApp } from './firebase';

const db = getFirestore(firebaseApp);

export async function createPost(post: IPostFormInputs): Promise<IPost> {
    const postRef = await addDoc(collection(db, 'posts'), post);

    return { ...post, id: postRef.id as string };
}

export async function getAllPosts() {
    const querySnapshot = await getDocs(collection(db, 'posts'));

    const posts: IPost[] = [];

    querySnapshot.forEach((doc) => {
        posts.push({ id: doc.id, ...doc.data() } as IPost);
    });

    return posts;
}

export const deletePost = async (id: string) =>
    await deleteDoc(doc(db, 'posts', id));

export const updatePost = async ({ id, ...post }: IPost): Promise<IPost> => {
    const postRef = doc(db, 'posts', id);
    const newPost = {
        ...post,
    };
    await setDoc(postRef, newPost, { merge: true });

    return { ...post, id };
};
