export type UserProperties = {
    id: string;
    name: string;
    email: string;
};

export type CreateUserProperties = Omit<UserProperties, 'id'>;

export type CreateUserWithCredentialsProperties = Omit<UserProperties, 'id'> & {
    password: string;
};

export type ISaveUser = UserProperties;
