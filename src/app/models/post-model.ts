export type IPost = {
    id: string;
    title: string;
    image?: string;
    content: string;
    tags?: string;
};

export type IPostFormInputs = Omit<IPost, 'id'>;
