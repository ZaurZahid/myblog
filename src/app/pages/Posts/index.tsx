import { Box, Button, Dialog, DialogContent, DialogTitle } from "@mui/material";

import Layout from "../../components/Layout/layout";
import { PostsList } from "../../components/Posts/PostsList";
import { SavePost } from "../../components/Posts/SavePost";
import useBoolean from "../../hooks/use-boolean";
import { useCreatePost } from "../../hooks/use-create-post";
import { IPostFormInputs } from "../../models/post-model";
import styles from './posts.module.scss'

function Posts() {
    const {
        value: isOpen,
        setFalse: handleClose,
        setTrue: handleOpen,
    } = useBoolean(false);

    const { isLoading, mutateAsync, error } = useCreatePost();

    const onSubmit = async (post: IPostFormInputs, selectedLogo: any = null) => {
        try {
            await mutateAsync({ ...post, image: selectedLogo });
            handleClose();
        } catch (err) { }
    };

    return (
        <Layout>
            <div className={styles['container']}>
                <Dialog open={isOpen} onClose={handleClose}>
                    <DialogTitle className='heading-text'>Add new Post</DialogTitle>
                    <DialogContent>
                        <SavePost
                            onSubmit={onSubmit}
                            isLoading={isLoading}
                            error={error?.message}
                        />
                    </DialogContent>
                </Dialog>

                <Button variant="contained" color='secondary' onClick={handleOpen} size="large">
                    Add new post
                </Button>

                <Box sx={{ marginTop: 2 }}>
                    <PostsList />
                </Box>
            </div>
        </Layout >
    )
}

export default Posts