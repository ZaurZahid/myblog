import { yupResolver } from '@hookform/resolvers/yup';
import { Box, Button, TextField } from '@mui/material';
import { useContext, useState } from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';

import { AuthContext } from '../../../context/auth';
import { signInSchema } from '../../../utils/validation-form';
import styles from './signin.module.scss'

type Inputs = {
    email: string;
    password: string;
};

function SignIn() {
    const navigate = useNavigate()
    const [disabled, setDisabled] = useState(false)
    const [errorMessage, setErrorMessage] = useState('')
    const { register, handleSubmit, formState: { errors = {} } } = useForm<Inputs>({
        resolver: yupResolver(signInSchema)
    });

    const { signIn } = useContext(AuthContext);

    const onSubmit: SubmitHandler<Inputs> = async data => {
        setDisabled(true)

        try {
            const res: any = await signIn(data.email, data.password);
            if (!res) {
                setErrorMessage('Something went wrong')
            }
        } catch (err) {
            setErrorMessage(err.message)
        } finally {
            setDisabled(false)
        }
    }

    const navigateSignUp = () => {
        navigate('/sign-up')
    }

    return (
        <div className={styles['container']}>
            <Box
                component="form"
                onSubmit={handleSubmit(onSubmit)}
                noValidate
                autoComplete="signinform"
                className={styles['form-container']}
            >
                <h1 className={`large-text ${styles['header']}`}>Sign In</h1>
                <TextField
                    {...register('email', {
                        required: true,
                        pattern: {
                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                            message: "invalid email address"
                        }
                    })}
                    id="email"
                    type="email"
                    label="Email"
                    className={styles['form-input']}
                    error={Boolean(errors?.email)}
                />
                {errors.email?.message && <p className={styles['error-message']}>
                    {errors.email?.message}
                </p>}
                <TextField
                    {...register('password', { required: true })}
                    required
                    type="password"
                    id="password"
                    label="Password"
                    className={styles['form-input']}
                    error={Boolean(errors?.password)}
                />
                {errors.password?.message && <p className={styles['error-message']}>
                    {errors.password?.message}
                </p>}
                {!!errorMessage && <p className={styles['error-message']}>
                    {errorMessage}
                </p>}
                <Button
                    type="submit"
                    variant="contained"
                    style={{ marginTop: 20 }}
                    color='secondary'
                    disabled={disabled}
                >
                    Sign In
                </Button>
                <br />
                <div className={`${styles['navigate-signin']} ${disabled ? styles['navigate-signin-disabled'] : ""}`}>
                    <a onClick={navigateSignUp}>
                        Don&apos;t have an account ?
                    </a>
                </div>
            </Box>
        </div >
    )
}

export default SignIn