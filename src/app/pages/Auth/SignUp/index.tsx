import { yupResolver } from '@hookform/resolvers/yup';
import {
    Box,
    Button,
    TextField,
} from '@mui/material';
import { useContext, useState } from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';

import { AuthContext } from '../../../context/auth';
import { useCreateUser } from '../../../hooks/use-create-user';
import { signUpSchema } from '../../../utils/validation-form';
import styles from './signup.module.scss'

type Inputs = {
    name: string;
    email: string;
    password: string;
};

function Signup() {
    const navigate = useNavigate()
    const [errorMessage, setErrorMessage] = useState('')

    const { register, handleSubmit, formState: { errors = {} } } = useForm<Inputs>({
        resolver: yupResolver(signUpSchema)
    });
    const { mutateAsync, isLoading, error } = useCreateUser();
    const { signIn } = useContext(AuthContext);

    const onSubmitForm: SubmitHandler<Inputs> = async user => {
        try {
            await mutateAsync({ ...user });
            const res: any = await signIn(user.email, user.password);
            if (!res) {
                setErrorMessage('Something went wrong')
            }
        } catch (err) { }
    };

    const navigateSignIn = () => {
        navigate('/sign-in')
    }

    return (
        <div className={styles['container']}>
            <Box
                component="form"
                onSubmit={handleSubmit(onSubmitForm)}
                noValidate
                autoComplete="signupform"
                className={styles['form-container']}
            >
                <h1 className={`large-text ${styles['header']}`}>Sign Up</h1>
                <TextField
                    {...register('name', { required: true })}
                    required
                    id="name"
                    type="text"
                    label="Name"
                    className={styles['form-input']}
                    error={Boolean(errors?.name)}
                />
                {errors.name?.message && <p className={styles['error-message']}>
                    {errors.name?.message}
                </p>}
                <TextField
                    {...register('email', {
                        required: true
                    })}
                    id="email"
                    type="email"
                    label="Email"
                    className={styles['form-input']}
                    error={Boolean(errors?.email)}
                />
                {errors.email?.message && <p className={styles['error-message']}>
                    {errors.email?.message}
                </p>}
                <TextField
                    {...register('password', { required: true })}
                    required
                    type="password"
                    id="password"
                    label="Password"
                    className={styles['form-input']}
                    error={Boolean(errors?.password)}
                />
                {errors.password?.message && <p className={styles['error-message']}>
                    {errors.password?.message}
                </p>}
                {error?.message && <p className={styles['error-message']}>
                    {error?.message}
                </p>}
                {!!errorMessage && <p className={styles['error-message']}>
                    {errorMessage}
                </p>}
                <Button
                    type="submit"
                    variant="contained"
                    style={{ marginTop: 20 }}
                    disabled={isLoading}
                    color='secondary'
                >
                    Create account
                </Button>
                <br />
                <div className={`${styles['navigate-signup']} ${isLoading ? styles['navigate-signup-disabled'] : ""}`}>
                    <a onClick={navigateSignIn}>
                        Already have an account ?
                    </a>
                </div>
            </Box>
        </div >
    )
}

export default Signup