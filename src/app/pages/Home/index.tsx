import React from 'react'

import Logo from '../../../assets/images/logo.png'
import Layout from '../../components/Layout/layout'
import styles from './home.module.scss'

function Home() {
    return (
        <Layout>
            <div className={styles['container']}>
                <img className={styles['logo']} src={Logo} alt="logo" />
                <h1>Welcome to our blog creator website</h1>
                <p>Veniam libero enim, eius dicta necessitatibus nam beatae at quos quod sapiente? Odit esse quisquam quam omnis eius delectus ea, recusandae at explicabo placeat deleniti tenetur porro corporis, soluta adipisci!
                    Veniam libero enim, eius dicta necessitatibus nam beatae at quos quod sapiente? Odit esse quisquam quam omnis eius delectus ea, recusandae at explicabo placeat deleniti tenetur porro corporis, soluta adipisci!
                    Veniam libero enim, eius dicta necessitatibus nam beatae at quos quod sapiente? Odit esse quisquam quam omnis eius delectus ea, recusandae at explicabo placeat deleniti tenetur porro corporis, soluta adipisci!
                    Veniam libero enim, eius dicta necessitatibus nam beatae at quos quod sapiente? Odit esse quisquam quam omnis eius delectus ea, recusandae at explicabo placeat deleniti tenetur porro corporis, soluta adipisci!
                    Veniam libero enim, eius dicta necessitatibus nam beatae at quos quod sapiente? Odit esse quisquam quam omnis eius delectus ea, recusandae at explicabo placeat deleniti tenetur porro corporis, soluta adipisci!
                    Veniam libero enim, eius dicta necessitatibus nam beatae at quos quod sapiente? Odit esse quisquam quam omnis eius delectus ea, recusandae at explicabo placeat deleniti tenetur porro corporis, soluta adipisci!
                    Veniam libero enim, eius dicta necessitatibus nam beatae at quos quod sapiente? Odit esse quisquam quam omnis eius delectus ea, recusandae at explicabo placeat deleniti tenetur porro corporis, soluta adipisci!
                    Veniam libero enim, eius dicta necessitatibus nam beatae at quos quod sapiente? Odit esse quisquam quam omnis eius delectus ea, recusandae at explicabo placeat deleniti tenetur porro corporis, soluta adipisci!
                    Veniam libero enim, eius dicta necessitatibus nam beatae at quos quod sapiente? Odit esse quisquam quam omnis eius delectus ea, recusandae at explicabo placeat deleniti tenetur porro corporis, soluta adipisci!
                    Veniam libero enim, eius dicta necessitatibus nam beatae at quos quod sapiente? Odit esse quisquam quam omnis eius delectus ea, recusandae at explicabo placeat deleniti tenetur porro corporis, soluta adipisci!
                    Veniam libero enim, eius dicta necessitatibus nam beatae at quos quod sapiente? Odit esse quisquam quam omnis eius delectus ea, recusandae at explicabo placeat deleniti tenetur porro corporis, soluta adipisci!
                    Veniam libero enim, eius dicta necessitatibus nam beatae at quos quod sapiente? Odit esse quisquam quam omnis eius delectus ea, recusandae at explicabo placeat deleniti tenetur porro corporis, soluta adipisci!
                    Veniam libero enim, eius dicta necessitatibus nam beatae at quos quod sapiente? Odit esse quisquam quam omnis eius delectus ea, recusandae at explicabo placeat deleniti tenetur porro corporis, soluta adipisci!
                    Nobis ex saepe voluptatum cumque, dolores atque autem quo aliquid cupiditate nemo omnis ipsam placeat numquam rerum minima iusto ea! Fugiat neque illum deleniti placeat! Magnam explicabo placeat deleniti ullam.
                    Ullam at illum adipisci odit inventore laborum iure iusto sequi, perferendis aspernatur incidunt! Culpa dolorum ratione cum laborum obcaecati tenetur eos unde aspernatur iure reprehenderit, tempore ea repellat blanditiis perferendis?
                    Fugit aperiam assumenda molestiae placeat numquam ipsa quis consequuntur vitae officia quo voluptates, modi expedita sequi, ullam adipisci earum accusamus eaque odio necessitatibus laborum quasi dolore? Natus quod quisquam vitae.
                    Nesciunt quod perferendis placeat molestias maxime enim dolorem, fugiat quos, cumque id repudiandae delectus accusantium iste exercitationem odio voluptatibus nam illum blanditiis? Ea ad obcaecati illum atque debitis doloribus quidem!
                    Eos ipsum nihil quidem ex dignissimos ut quasi. Quis aperiam doloribus, saepe ratione enim, minima repellat tempora praesentium porro repudiandae placeat laudantium et ipsum, ducimus ad nobis sit deleniti error.</p>
            </div>
        </Layout >
    )
}

export default Home