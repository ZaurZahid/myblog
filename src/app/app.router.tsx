import React from 'react'
import { QueryClientProvider } from 'react-query';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

import { AuthProvider } from './context/auth';
import SignIn from './pages/Auth/SignIn/index';
import SignUp from './pages/Auth/SignUp';
import Home from './pages/Home/index';
import Posts from './pages/Posts/index';
import { queryClient } from './services/react-query';
import { AuthWrapper, PrivateWrapper } from './utils/Auth';
import ScrollToTop from './utils/ScrollToTop'

export default function AppRouter() {
    return (
        <Router>
            <QueryClientProvider client={queryClient}>
                <AuthProvider>
                    <ScrollToTop>
                        <Routes>
                            <Route element={<AuthWrapper />}>
                                <Route path="sign-in" element={<SignIn />} />
                                <Route path="sign-up" element={<SignUp />} />
                            </Route>

                            <Route element={<PrivateWrapper />}>
                                <Route path="" element={<Home />} />
                                <Route path="posts" element={<Posts />} />
                            </Route>

                            <Route path="*" element={<p>unknown</p>} />
                        </Routes>
                    </ScrollToTop>
                </AuthProvider>
            </QueryClientProvider>
        </Router>
    )
}
