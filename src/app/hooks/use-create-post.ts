import { FirebaseError } from 'firebase/app';
import { useMutation } from 'react-query';

import { IPost, IPostFormInputs } from '../models/post-model';
import { createPost } from '../services/post-service';
import { queryClient } from '../services/react-query';

export function useCreatePost() {
    return useMutation<IPost, FirebaseError, IPostFormInputs>(
        (post) => createPost(post),
        {
            onMutate: (newPost) => {
                const oldPosts = queryClient.getQueryData<IPost[]>('posts');

                queryClient.setQueryData<IPost[]>('posts', (oldPosts = []) => [
                    ...oldPosts,
                    { ...newPost, id: '' },
                ]);

                return () => oldPosts;
            },
            onError: (error, post, rollback: any) => {
                if (rollback) rollback();
            },
            onSettled: () => {
                queryClient.invalidateQueries('posts');
            },
        },
    );
}
