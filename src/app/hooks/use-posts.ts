import { FirebaseError } from 'firebase/app';
import { useQuery } from 'react-query';

import { IPost } from '../models/post-model';
import { getAllPosts } from '../services/post-service';

export function usePosts() {
    return useQuery<IPost[], FirebaseError>('posts', () => getAllPosts());
}
