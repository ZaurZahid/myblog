import { FirebaseError } from 'firebase/app';
import { useMutation } from 'react-query';

import { IPost } from '../models/post-model';
import { updatePost } from '../services/post-service';
import { queryClient } from '../services/react-query';

export function useSavePost() {
    return useMutation<IPost, FirebaseError, IPost>(
        (post) => updatePost(post),
        {
            onSuccess: (updatedPost) => {
                queryClient.setQueryData<IPost[]>('posts', (posts = []) =>
                    posts.map((post) =>
                        post.id === updatedPost.id ? updatedPost : post,
                    ),
                );
            },
        },
    );
}
