import { FirebaseError } from 'firebase/app';
import { useMutation } from 'react-query';

import { IPost } from '../models/post-model';
import { deletePost } from '../services/post-service';
import { queryClient } from '../services/react-query';

export function useDeletePost() {
    return useMutation<void, FirebaseError, string>((id) => deletePost(id), {
        onMutate: (postId) => {
            const oldPosts = queryClient.getQueryData<IPost[]>('posts');

            queryClient.setQueryData<IPost[]>('posts', (oldPosts = []) =>
                oldPosts.filter((oldPost) => oldPost?.id !== postId),
            );

            return () => oldPosts;
        },
        onError: (error, post, rollback: any) => {
            if (rollback) rollback();
        },
        onSettled: () => {
            queryClient.invalidateQueries('posts');
        },
    });
}
