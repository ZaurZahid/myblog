import { Delete as DeleteIcon, Edit as EditIcon } from '@mui/icons-material';
import {
    Box,
    Dialog,
    DialogContent,
    DialogTitle,
    IconButton,
    Paper,
    Snackbar,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow
} from '@mui/material';
import { useState } from 'react';
import { v4 } from 'uuid';

import useBoolean from '../../../hooks/use-boolean';
import { useDeletePost } from '../../../hooks/use-delete-post';
import { usePosts } from '../../../hooks/use-posts';
import { useSavePost } from '../../../hooks/use-save-post';
import { IPost, IPostFormInputs } from '../../../models/post-model';
import DeletePopup from '../../DeletePopup';
import { SavePost } from '../SavePost';
import styles from './posts-list.module.scss'

export function PostsList() {
    const { data: posts } = usePosts();
    const { mutateAsync: deletePost } = useDeletePost();
    const {
        isLoading: isPostLoading,
        error: updatePostError,
        mutateAsync: updatePost,
    } = useSavePost();

    const [selectedPost, setSelectedPost] = useState<IPost>(
        {} as IPost,
    );

    const [popup, setPopup] = useState({
        show: false,
        id: null,
    });

    const {
        value: isOpenPostEditDialog,
        setFalse: handleClosePostEditDialog,
        setTrue: openPostModal,
    } = useBoolean(false);

    const [snackbarOptions, setSnackbarOptions] = useState({
        isOpen: false,
        message: '',
    });

    const handleCloseSnackbar = () =>
        setSnackbarOptions(old => ({ ...old, isOpen: false }));

    async function handleDeletePost(id: string) {
        setPopup({
            show: true,
            id,
        });
    }

    const handleDeleteTrue = async () => {
        if (popup.show && popup.id) {
            try {
                await deletePost(popup.id);

                setSnackbarOptions({
                    message: 'Post deleted',
                    isOpen: true,
                });
                setPopup({
                    show: false,
                    id: null,
                });
            } catch (error: any) { }
        }
    };

    const handleDeleteFalse = () => {
        setPopup({
            show: false,
            id: null,
        });
    };

    async function handleUpdatePost(post: IPost) {
        setSelectedPost(post);

        openPostModal();
    }

    async function onSubmitPostForm(post: IPostFormInputs) {
        try {
            await updatePost({ id: selectedPost.id, ...post });

            setSnackbarOptions({
                message: 'Post updated',
                isOpen: true,
            });

            handleClosePostEditDialog();
        } catch (error: any) {
            setSnackbarOptions({
                message: error.message,
                isOpen: true,
            });
        }
    }

    return (
        <>
            <Dialog open={isOpenPostEditDialog} onClose={handleClosePostEditDialog}>
                <DialogTitle className='heading-text'>Update Post</DialogTitle>
                <DialogContent>
                    <SavePost
                        onSubmit={onSubmitPostForm}
                        isLoading={isPostLoading}
                        error={updatePostError?.message}
                        defaultValues={selectedPost}
                    />
                </DialogContent>
            </Dialog>
            <DeletePopup
                open={popup.show}
                handleDeleteTrue={handleDeleteTrue}
                handleDeleteFalse={handleDeleteFalse}
            />
            <Snackbar
                open={snackbarOptions.isOpen}
                autoHideDuration={6000}
                onClose={handleCloseSnackbar}
                anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
                message={snackbarOptions?.message}
            />
            {posts?.map(post =>
                <div key={v4()}>
                    <h1>{post.title}</h1>
                    <p>{post.content}</p>
                    <div>
                        <IconButton
                            onClick={() => handleDeletePost(post.id)}
                            edge="end"
                            aria-label="delete"
                            sx={{ marginRight: 1 }}
                        >
                            <DeleteIcon />
                        </IconButton>
                        <IconButton
                            onClick={() => handleUpdatePost(post)}
                            edge="end"
                            aria-label="edit"
                        >
                            <EditIcon />
                        </IconButton>
                    </div>
                </div>
            )}
        </>
    );
}
