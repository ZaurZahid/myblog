import { yupResolver } from "@hookform/resolvers/yup";
import { Box, Button, TextField, } from "@mui/material";
import { useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";

import { IPostFormInputs } from "../../../models/post-model";
import { savePostSchema } from "../../../utils/validation-form";
import PostImage from "./PostImage/post-image";
import styles from './save-post.module.scss'

export type PostFormProperties = {
    defaultValues?: IPostFormInputs;
    onSubmit: (user: IPostFormInputs, selectedLogo?: File) => void;
    error?: string;
    isLoading: boolean;
};

export const SavePost = ({
    error,
    isLoading,
    defaultValues,
    onSubmit,
}: PostFormProperties) => {
    const {
        register,
        handleSubmit,
        formState: { errors = {} }
    } = useForm<IPostFormInputs>({
        defaultValues,
        resolver: yupResolver(savePostSchema)
    });

    const toBase64 = file => new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });

    const fromBase64 = (base64) => fetch(base64)
        .then(res => res.blob())
        .then(blob => {
            const file = new File([blob], "File name", { type: "image/png" })
            console.log(file)
            return file
        })

    const getFormData: SubmitHandler<IPostFormInputs> = user => onSubmit(user, seletedLogo)
    const [seletedLogo, setSeletedLogo] = useState(null)
    const [previewLogo, setPreviewLogo] = useState(defaultValues?.image ? fromBase64(defaultValues?.image) : "");
    const [invalidImageMessage, setInvalidImageMessage] = useState("");

    const handleFile = async file => {
        if (!file) {
            setInvalidImageMessage('Please select image.')
            return false;
        }

        if (!file.name.match(/\.(png|jpg|jpeg)$/)) {
            setInvalidImageMessage('Please select valid image.')
            return false;
        }

        if (!file.name.match(/\.(png|jpg|jpeg)$/)) {
            setInvalidImageMessage('Please select valid image.')
            return false;
        }

        if (!(file.size / 1024 / 1024 < 2)) {
            setInvalidImageMessage('Image must smaller than 2MB!')
            return false;
        }

        setInvalidImageMessage('')
        setSeletedLogo(await toBase64(file));
        setPreviewLogo(URL.createObjectURL(file));
    }

    return (
        <section>
            <Box
                component="form"
                onSubmit={handleSubmit(getFormData)}
                style={{ width: '50ch', maxWidth: "70vw" }}
                sx={{
                    '& .MuiPaper-root': { m: 1, width: '100%' },
                    '& .MuiFormControl-root': { m: 1 },
                    '& .MuiFormGroup-root': { m: 1 },
                }}
                noValidate
                autoComplete="off"
            >
                <Box style={{ flexDirection: 'column', display: 'flex' }}>
                    <TextField
                        {...register('title', { required: true })}
                        required
                        id="title"
                        type="text"
                        label="Title"
                        fullWidth
                        autoFocus={true}
                        error={Boolean(errors.title?.message)}
                    />
                    <p className={styles['error-message']}>
                        {errors.title?.message}
                    </p>
                    <TextField
                        {...register('content', { required: true })}
                        required
                        id="content"
                        type="text"
                        label="Content"
                        fullWidth
                        error={Boolean(errors.content?.message)}
                        multiline
                        maxRows={6}
                        minRows={4}
                    />
                    <p className={styles['error-message']}>
                        {errors.content?.message}
                    </p>
                    <div className={styles['form-company-logo-container']}>
                        <PostImage
                            previewLogo={previewLogo}
                            handleFile={handleFile}
                            invalidImageMessage={invalidImageMessage}
                        />
                    </div>
                    <p className={styles['error-message']}>
                        {error}
                    </p>
                    <Box style={{ margin: '8px' }}>
                        <Button
                            disabled={isLoading}
                            variant="contained"
                            type="submit"
                            color='secondary'
                        >
                            Save
                        </Button>
                    </Box>
                </Box>
            </Box>
        </section>
    );
};
