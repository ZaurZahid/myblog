import { Button } from '@mui/material';
import { useRef } from 'react';

import styles from './post-image.module.scss'

interface IPostImageProps {
    previewLogo: any;
    handleFile: (file: any) => void;
    invalidImageMessage: string
}

const PostImage: React.FC<IPostImageProps> = ({ previewLogo, invalidImageMessage, handleFile }) => {
    const fileInput = useRef(null);
    const handleDragOver = (e: any) => {
        e.preventDefault();
    }
    const handleOnDrop = (e: any) => {
        e.preventDefault();
        e.stopPropagation();
        const imageFile = e.dataTransfer.files[0];
        handleFile(imageFile)
    }

    const handleBrowseFiles = () => {
        fileInput.current.click()
    }

    return (
        <div className={styles['logo-container']}>
            <div
                className={styles['select-logo-container']}
                onDragOver={handleDragOver}
                onDrop={handleOnDrop}
            >
                <div className={styles['select-logo-box']}>
                    <p className={`body-text ${styles['drag-text']}`}>Drag & Drop your logo here</p>
                    <p className={`body-text ${styles['drag-text-or']}`}>OR</p>
                    <div className={styles['select-logo-button']}>
                        <input
                            type="file"
                            accept=".png,.jpg,.jpeg"
                            // accept='image/*'
                            ref={fileInput}
                            onChange={e => handleFile(e.target.files[0])}
                        />
                        <Button onClick={handleBrowseFiles}>Browse Files</Button>
                    </div>
                    {invalidImageMessage ? <span className={`body-text-small ${styles['error-message']}`}>{invalidImageMessage}</span> : null}
                </div>
            </div>

            {previewLogo
                ? <div className={styles['current-logo-container']}>
                    <h5 className='body-text-heading'>Current logo:</h5>
                    <div className={styles['current-logo']}>
                        <img src={previewLogo} alt="selected logo" />
                    </div>
                </div>
                : null}
        </div>
    )
}

export default PostImage