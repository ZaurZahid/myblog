import HomeIcon from '@mui/icons-material/Home';
import LocalPostOfficeIcon from '@mui/icons-material/LocalPostOffice';
import LogoutIcon from '@mui/icons-material/Logout';
import MenuIcon from '@mui/icons-material/Menu';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import Toolbar from '@mui/material/Toolbar';
import * as React from 'react';
import { useContext } from 'react';
import { NavLink } from 'react-router-dom';

import { AuthContext } from '../../context/auth';
import styles from './layout.module.scss';

const drawerWidth = 195;


export default function Layout({ children }) {
    const { user } = React.useContext(AuthContext);
    const { logout } = useContext(AuthContext);

    const [mobileOpen, setMobileOpen] = React.useState(false);
    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const MENU_ITEMS = [
        { title: 'Home', link: '/', icon: <HomeIcon /> },
        { title: 'Posts', link: '/posts', icon: <LocalPostOfficeIcon /> },
    ];

    const drawer = (
        <div className={styles['drawer-content']}>
            <div className={styles['top-part']}>
                <div className='body-text' style={{ textAlign: "center", marginBottom: 50, cursor: "default" }}>Hey <strong>{user.name}</strong> ;)</div>
                <ul className={styles['list']}>
                    {MENU_ITEMS.map(({ title, link, icon }, index) => (
                        <li
                            key={index}
                            className={`${styles['list-item']}`}
                        >
                            <NavLink
                                to={link}
                                className={styles['list-link']}
                            >
                                {icon}
                                {title}
                            </NavLink>
                        </li>
                    ))}
                </ul>
            </div>
            <div className={`body-text ${styles['logout-item']}`} onClick={logout}>
                <LogoutIcon />
                Logout
            </div>
        </div >
    );

    return (
        <Box sx={{ display: 'flex' }}>
            <AppBar
                position="fixed"
                sx={{
                    display: { xs: 'block', lg: 'none' },
                    backgroundColor: 'white',
                    boxShadow: '0px 0px 16px rgba(82, 95, 107, 0.2) !important',
                }}
            >
                <Toolbar className={styles['appbar-toolbar']}>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        sx={{ mr: 2, display: { lg: 'none' } }}
                    >
                        <MenuIcon style={{ color: '#525F6B' }} />
                    </IconButton>
                    <div className={`body-text ${styles['toolbar-right-mobile']}`}>
                        <div className={`${styles['logout-item-mobile']}`} onClick={logout}>
                            <LogoutIcon />
                        </div>
                    </div>
                </Toolbar>
            </AppBar>
            <Box
                component="nav"
                sx={{ width: { lg: drawerWidth }, flexShrink: { lg: 0 } }}
                aria-label="list of pages"
            >
                {/* tablet and mobile */}
                <Drawer
                    variant="temporary"
                    open={mobileOpen}
                    onClose={handleDrawerToggle}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                    sx={{
                        display: { xs: 'block', lg: 'none' },
                        '& .MuiDrawer-paper': {
                            boxSizing: 'border-box',
                            width: drawerWidth,
                            padding: '0 20px',
                            border: 'none',
                            boxShadow: '0px 0px 16px rgba(107, 88, 82, 0.2)',
                        },
                    }}
                >
                    {drawer}
                </Drawer>
                {/* desktop */}
                <Drawer
                    variant="permanent"
                    sx={{
                        display: { xs: 'none', lg: 'block' },
                        '& .MuiDrawer-paper': {
                            boxSizing: 'border-box',
                            width: drawerWidth,
                            padding: '0 15px',
                            border: 'none',
                            boxShadow: '0px 0px 16px rgba(107, 88, 82, 0.2)',
                        },
                    }}
                    open
                >
                    {drawer}
                </Drawer>
            </Box>
            <Box
                component="main"
                sx={{
                    flexGrow: 1,
                    width: { sm: `calc(100% - ${drawerWidth}px)` },
                    padding: {
                        xs: '40px 16px 40px 16px',
                        sm: '40px 24px 40px 24px',
                        lg: '5px 80px 80px 80px',
                    },
                }}
            >
                <Toolbar className={styles['content-top-toolbar']} />
                {children}
            </Box>
        </Box>
    );
}
