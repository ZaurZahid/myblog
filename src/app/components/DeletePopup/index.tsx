import { Box, Button, Modal, Typography } from '@mui/material';
import React from 'react'

import styles from './delete-popup.module.scss'

interface IDeletePopup {
    open: boolean;
    handleDeleteTrue: () => void;
    handleDeleteFalse: () => void;
}

const modalStyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    minWidth: '30%'
};

const DeletePopup: React.FC<IDeletePopup> = ({
    open,
    handleDeleteTrue,
    handleDeleteFalse
}) => {
    return (
        <Modal
            open={open}
            onClose={handleDeleteFalse}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={modalStyle}>
                <Typography variant="h5" gutterBottom component="div">
                    Are you sure to remove?
                </Typography>
                <Box sx={{ marginTop: 4 }} className={styles['modal-actions']}>
                    <Button
                        variant="contained"
                        color='inherit'
                        onClick={handleDeleteFalse}
                    >
                        Decline
                    </Button>
                    <Button
                        variant="contained"
                        color='warning'
                        style={{ marginLeft: 20 }}
                        onClick={handleDeleteTrue}
                    >
                        Confirm
                    </Button>
                </Box>
            </Box>
        </Modal>
    )
}

export default DeletePopup