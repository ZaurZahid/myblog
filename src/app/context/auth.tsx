import { getAuth, signOut } from 'firebase/auth';
import React, { createContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { UserProperties } from '../models/user-model';
import { getUserByEmail, login } from '../services/user-service';
import { isEmpty } from '../utils/util';

interface AuthContextProperties {
  signIn: (email: string, password: string) => Promise<void>;
  logout: () => void;
  user: UserProperties;
}

interface IAuthProvider {
  children: any;
}

export const AuthContext = createContext<AuthContextProperties>(
  {} as AuthContextProperties,
);

export const AuthProvider: React.FC<IAuthProvider> = ({ children }) => {
  const navigate = useNavigate()
  const [user, setUser] = useState({} as UserProperties);

  async function signIn(email: string, password: string) {
    try {
      const response = await getUserByEmail(email);
      if (!isEmpty(response)) {
        await login(email, password);
        window.localStorage.setItem('blog-auth', JSON.stringify(response))

        setUser(response as UserProperties);

        navigate('/', { replace: true });
      }
    } catch (err) {
    }
  }

  async function logout() {
    const auth = getAuth()
    await signOut(auth);
    window.localStorage.removeItem('blog-auth')

    setUser({
      id: '',
      name: '',
      email: '',
    });

    navigate('/sign-in', { replace: true });
  }

  useEffect(() => {
    let authParsed: UserProperties = {
      id: '',
      name: '',
      email: '',
    }

    const auth = window.localStorage.getItem('blog-auth')

    try {
      if (!auth) {
        navigate('/sign-in');
        return
      }
      authParsed = JSON.parse(auth);

      const userParsed = {
        name: authParsed?.name,
        email: authParsed?.email,
        id: authParsed?.id,
      };

      setUser(userParsed);
    } catch (err) {
      navigate('/sign-in');
    }
  }, []);

  return (
    <AuthContext.Provider value={{ signIn, user, logout }}>
      {children}
    </AuthContext.Provider>
  );
};