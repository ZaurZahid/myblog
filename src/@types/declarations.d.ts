// styles
declare module '*.scss';
declare module '*.module.scss';

// images
declare module '*.jpg' {
    const src: string;
    export default src;
}
declare module '*.jpeg' {
    const src: string;
    export default src;
}
declare module '*.png' {
    const src: string;
    export default src;
}
declare module '*.svg' {
    const src: string;
    export default src;
}
